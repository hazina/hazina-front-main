import Vue from 'vue'
import VueRouter, { RouteConfig } from "vue-router";
import store from '../store'
import Home from '../views/Home/Home.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/login',
        name: 'Login',
        component: ()=> import('../views/Login/Login.vue')
    },
    {
        path: '/register',
        name: 'Register',
        component: ()=> import('../views/Register/Register.vue')
    },

    {
        path: '/forgot-pasword',
        name: 'ForgotPassword',
        component: ()=> import('../views/ForgotPassword/ForgotPassword.vue')
    },
    {
        path: '/reset-password/:token',
        name: 'ResetPassword',
        component: ()=> import('../views/ResetPassword/ResetPassword.vue')
    },
    {
        path: '/verify/phone/:id',
        name: 'VerifyPhone',
        component: ()=> import('../views/VerifyPhone/VerifyPhone.vue')
    },
    {
        path: '/account/verify/:token',
        name: 'VerifyEmail',
        component: ()=> import('../views/VerifyEmail/VerifyEmail.vue')
    },
    {
        path: '/leads/welcome/:token',
        name: 'LeadsWelcome',
        component: ()=> import('../views/LeadsWelcome/LeadsWelcome.vue')
    },
    {
        path: '/email/resend-verification',
        name: 'ResendVerification',
        component: ()=> import('../views/ResendVerification/ResendVerification.vue')
    },

]

const router = new VueRouter({
    mode: 'history',
    //base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
});

export default router
