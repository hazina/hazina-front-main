import Vue from "vue";
import App from "./views/App/App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Axios from "axios";
import Vuelidate from "vuelidate";
import Toasted from "vue-toasted";
//const VueCountdownTimer = require("vuejs-countdown-timer")
//const VueTelInputVuetify = require("vue-tel-input-vuetify");
import VueTelInputVuetify from "vue-tel-input-vuetify/lib";
import VueMeta from 'vue-meta'

Vue.config.productionTip = true
Vue.use(vuetify);
Vue.use(VueTelInputVuetify, {
    vuetify,
});

Vue.use(Vuelidate);
Vue.use(Toasted);
Vue.use(VueMeta)

// Vue.use(VueCountdownTimer);

Vue.prototype.$http = Axios;
const token = localStorage.getItem('haziAuth')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer '+token
}
Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer K3r1WjgalH7Og4A0aHL1nV7bk0sTT09aoq7SnAAS2ir1XOH22GBgg3QU8CUs';
Vue.prototype.$http.defaults.baseURL = 'https://api.gethazina.com/v1';

// Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer amfmw136hC1RjhdZCamMeiN8x349utATvrcXWJo8vfUQRKHYnGGxJQLxQ9JI';
// Vue.prototype.$http.defaults.baseURL = 'https://hazinang.dev/v1';

new Vue({
  router,
  store,  
  vuetify,
  render: h => h(App)
}).$mount("#app");
