import { Component, Prop, Vue } from 'vue-property-decorator';
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import store from "../../store";

@Component({
    validations: {
        password: { required, minLength: minLength(6) },
        confirm_password: {
            required,
            minLength: minLength(6),
            sameAs: sameAs("password")
        }
    },
})
export default class LeadsWelcome extends Vue {

    isLoading: boolean = false;
    hasError: boolean = false;
    errorMessages: string = "";

    password: string = "";
    confirm_password: string = "";


    submit() {
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let sortParam = this.$route.params.token.split('-');


            let data = {
                password: this.password,
                code: sortParam[1],
                user_id: sortParam[0]
            };
            let that = this;

            axios
                .post("/consumer/auth/create-password", data)
                .then(resp => {
                    this.isLoading = false;
                    this.$toasted.success("You password has been updated successfully!")
                    store.dispatch("token_login", resp.data.token).then(() => {
                        window.location.href = "/portal/consumer";
                    });
                })
                .catch(err => {
                    this.hasError = true;
                    if (
                        _.has(err.response.data, "errors") &&
                        typeof err.response.data.errors != "string"
                    ) {
                        var field = _.keys(err.response.data.errors);
                        this.errorMessages =
                            err.response.data.errors[field[0]][0];
                    } else {
                        this.errorMessages = err.response.data.errors;
                    }
                })
                .finally(function() {
                    that.isLoading = false;
                });
        }
    }

    get passwordErrors() {
        const errors: Array<string> = [];
        if (!this.$v.password.$dirty) return errors;
        !this.$v.password.required && errors.push("Password is required");
        !this.$v.password.minLength &&
            errors.push("Password must be at least 4 characters long");
        //!this.$v.password.sameAs && errors.push('Passwords do not match')
        return errors;
    }
    get confimPasswordErrors() {
        const errors: Array<string> = [];
        if (!this.$v.confirm_password.$dirty) return errors;
        !this.$v.confirm_password.required &&
            errors.push("You must confirm your password.");
        !this.$v.confirm_password.minLength &&
            errors.push("Password must be at least 4 characters long");
        !this.$v.confirm_password.sameAs &&
            errors.push("Passwords do not match");
        return errors;
    }

}
