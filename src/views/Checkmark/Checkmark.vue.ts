import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class Checkmark extends Vue {
  @Prop() public type!: string;
}
