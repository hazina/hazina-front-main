import { Component, Vue } from "vue-property-decorator";
import Axios from "axios";
import store from "../../store";
import { required } from "vuelidate/lib/validators";
import * as _ from "lodash";
import CodeInput from "vue-verification-code-input";
declare let DuphluxPop: any;

// Route::post('/phone/verify', 'AuthCtrl@verify_phone');
// Route::post('/phone/send-code', 'AuthCtrl@send_code');
// Route::get('/phone/get-number', 'AuthCtrl@get_phone_number');
// Route::post('/phone/call/verify', 'AuthCtrl@call_verify');

@Component({
  components: { 
    CodeInput,
  },
  validations: {
    phone: { required },
  },
})
export default class VerifyPhone extends Vue {
  // @Prop() private msg!: string;
  shouldResendCode: boolean = false;
  isLoading: boolean = false;
  phone: string = "";
  code: string = "";
  errorMessages: string = "";
  hasError: boolean = false;
  isCalling: boolean = false; 
  isResending: boolean = false;
  isLoggingIn: boolean = false;
  duplux_token: string = "34792cda48f4f90736d3faed467503568b347ee0";
  topCountries: Array<string> = ["NG"];

  sendCode() {
    this.isResending = true; 
    this.sendCodeApi()
      .then((resp: any) => {
        this.isResending = false;
        this.$toasted
          .success("A new code has been sent to your phone")
          .goAway(5000);
      })
      .catch((error) => {
        this.isResending = false;
        this.$toasted
          .success("There was an error sending a new code")
          .goAway(5000);
      });
  }

  sendCodeNewNumber() {
    this.isLoading = true;
    this.sendCodeApi(this.phone)
      .then((resp: any) => {
        this.shouldResendCode = false;
        this.isLoading = false;
      })
      .catch((err) => {
        this.hasError = true;
        this.isLoading = false;
        if (
          _.has(err.response.data, "errors") &&
          typeof err.response.data.errors != "string"
        ) {
          var field = _.keys(err.response.data.errors);
          this.errorMessages = err.response.data.errors[field[0]][0];
        } else {
          this.errorMessages = err.response.data.errors;
        }
      });
  }

  callVerify() {
    this.isCalling = true;
    Axios.get("/consumer/auth/phone/get-number", {
      params: { id: this.$route.params.id },
    })
      .then((resp: any) => {
        //this.isCalling = false;
        //console.log(resp.data);
        this.initPhoneCall(resp.data.phone, resp.data.reference);
      })
      .catch((err) => {
        this.isCalling = false;
        this.errorMessages = !err ? "" : err.response.data.errors;
        //console.log(err);
      });
  }

  initPhoneCall(phone: String, reference: String) {
    let that = this;
    (<any>DuphluxPop).init({
      token: this.duplux_token, // This is found under your app in your dashboard.
      timeout: 900, // timeout in seconds
      phone_number: phone,
      redirect_url: window.location.href, // Or any custom url of your choice
      transaction_reference: reference, // A unique transaction reference to identity this authentication request
      callback: () => {
        return {
          onSuccess: function(reference: any) {
            Axios.post("/consumer/auth/call/verify", {
              id: that.$route.params.id,
            })
              .then((resp) => {
                that.isCalling = false;
                store.dispatch("token_login", resp.data.token).then(() => {
                  window.location.href = "/portal/consumer";
                });
              })
              .catch((err) => {
                that.isCalling = false;
                that.errorMessages = err.response.data.errors;
              });
          },
          onFailure: function(error: any) {
            that.isCalling = false;
            that.errorMessages =
              "Unable to verify your phone number, please try again";
          },
          onError: function(errorMessage: any) {
            that.isCalling = false;
            that.errorMessages =
              "Unable to verify your phone number, please try again";
          },
        };
      },
    });
    (<any>DuphluxPop).launch();
  }

  setPhone(evt: any) {
    this.code = evt;
  }

  Verify() {
    this.verifyApi()
      .then((resp: any) => {
        store.dispatch("token_login", resp.token).then(() => {
          this.isLoggingIn = true;
          window.location.href = "/portal/consumer";
        });
      })
      .catch((err) => {
        this.errorMessages = !err ? "" : err.response.data.errors;
      });
  }

  verifyApi() {
    this.isLoading = true;
    return new Promise((resolve, reject) => {
      Axios.post("/consumer/auth/phone/verify", {
        id: this.$route.params.id,
        code: this.code,
      })
        .then((resp) => {
          this.isLoading = false;
          resolve({ token: resp.data.token });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  sendCodeApi(phone: string = "") {
    return new Promise((resolve, reject) => {
      Axios.post("/consumer/auth/phone/send-code", {
        id: this.$route.params.id,
        phone: phone.replace(/\s+/g, ""),
      })
        .then((resp) => {
          resolve({ data: resp.data.data });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  get phoneErrors() {
    const errors: Array<string> = [];
    if (!this.$v.phone.$dirty) return errors;
    !this.$v.phone.required &&
      errors.push("You must provide your phone number.");
    return errors;
  }
}
