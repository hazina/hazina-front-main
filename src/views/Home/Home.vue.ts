import { Component, Vue } from 'vue-property-decorator';
import { mdiPhone, mdiMessageText, mdiEmail, mdiMapMarker, mdiPizza, mdiBaby, mdiEmoticonHappyOutline, mdiBroom, mdiHospitalBoxOutline, mdiResponsive, mdiChevronUp } from '@mdi/js'
// import store from '../../store';
// import Axios from 'axios';

@Component({
})

export default class Home extends Vue {

  //title: any = "Welcome to Hazina";

  phone: any = mdiPhone
  message: any = mdiMessageText
  email: any = mdiEmail
  map: any = mdiMapMarker
  pizza: any = mdiPizza
  baby: any = mdiBaby
  emot: any = mdiEmoticonHappyOutline
  broom: any = mdiBroom
  hospital: any = mdiHospitalBoxOutline
  responsive: any = mdiResponsive
  chevron_up: any = mdiChevronUp
 

  slides: any = [
    require('@/assets/images/slides/1.jpeg'),
    require('@/assets/images/slides/2.jpeg'),
    require('@/assets/images/slides/3.jpeg'),
  ];

  nitem = 1;
  pitem = 1;
  nitems: any = [
    { text: 'Beauty & Cosmetics', icon: this.emot },
    { text: 'Baby Care', icon: this.baby },
    { text: 'Food & Beverage', icon: this.pizza },
  ]

  pitems: any = [
    { text: 'Cleaning Supplies', icon: this.broom },
    { text: 'Health & Personal Care', icon: this.hospital },
    { text: 'Digital Products', icon: this.responsive },
    { text: 'Pet Care', icon: this.baby },
  ]


}
